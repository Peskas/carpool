import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrMenuPage } from './qr-menu.page';

describe('QrMenuPage', () => {
  let component: QrMenuPage;
  let fixture: ComponentFixture<QrMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrMenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
