import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { NgxQRCodeModule } from 'ngx-qrcode2';

// import {qr} from '../api/qr';


@Component({
  selector: 'app-qr-menu',
  templateUrl: './qr-menu.page.html',
  styleUrls: ['./qr-menu.page.scss'],
})
export class QrMenuPage implements OnInit {

  lat: number ;
  long: number ;
  qrData: string ;
  scanedCode = null ;
  createdCode = null ;


  constructor(private geolocation: Geolocation,
              private barcodeScanner: BarcodeScanner,
              private ngxqr: NgxQRCodeModule,
    ) { }

  ngOnInit() {
    this.getGeolocation();
  }

  getGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.long = resp.coords.longitude;
      console.log(this.lat);
      console.log(this.long);
    }).catch((error) => {
       console.log('Error getting location', error);
     });
    }

    displayQr() {
      // anadir usuario al qr
      this.qrData = this.lat.toString() + ' ' + this.long.toString()  ;
      console.log(this.qrData);
      this.createdCode = this.qrData;
    }

    scanQr() {
      this.barcodeScanner.scan().then(data => {
        this.scanedCode = data.text;

        // enviar info qr + token + coordenadas
      });
    }

}
