import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor( private  authService: AuthService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    console.log('chek log');
    this.authService.getToken().then(res => this.authService.checklog()) ;
  }

}
