import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule'},
  { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'register-code', loadChildren: './auth/register-code/register-code.module#RegisterCodePageModule' },
  { path: 'add-vehicle', loadChildren: './api/add-vehicle/add-vehicle.module#AddVehiclePageModule' },
  { path: 'forget-pass', loadChildren: './auth/forget-pass/forget-pass.module#ForgetPassPageModule' },
  { path: 'change-pass', loadChildren: './auth/change-pass/change-pass.module#ChangePassPageModule' },
  { path: 'recover-pass-code', loadChildren: './auth/recover-pass-code/recover-pass-code.module#RecoverPassCodePageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'qr-menu', loadChildren: './qr-menu/qr-menu.module#QrMenuPageModule' },
  { path: 'user-menu', loadChildren: './user-menu/user-menu.module#UserMenuPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
