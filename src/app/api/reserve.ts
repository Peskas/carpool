
export interface Reserve {
    mat: string;
    hours: number;
    minutes: number;
}
