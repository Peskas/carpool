import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';


import { AuthService } from '../auth/auth.service';
import { Vehicle } from './vehicle';
import { VehicleResponse } from './vehicle-response';
import { ReserveResponse } from './reserve-response';
import { QrResponse} from './qr-response' ;
import { Qr } from './qr';
import { Reserve } from './reserve';

@Injectable({
  providedIn: 'root'
})


export class ApiService {

  public mat: string = null;
  public API_SERVER_ADDRESS  =  'http://localhost:3000';

  constructor(
    private  httpClient: HttpClient,
    private  authService: AuthService,
    private  alertControler: AlertController) {}




regVehicle(vehicle: Vehicle): Observable<VehicleResponse> {

  return this.httpClient.post<Vehicle>(`${this.API_SERVER_ADDRESS}/regVehicle`, vehicle,
  {headers: new HttpHeaders({token : this.authService.jwt})
  }).pipe(
    tap(async (res: VehicleResponse) => {
      this.alertMatriculaRegistrada(res.mat);
    }, (err) => {
      if (err.status === 500) {this.alertServerError(); }
    })
  );
}



getVehicle(): Observable<Vehicle> {

  return this.httpClient.get<Vehicle>(`${this.API_SERVER_ADDRESS}/getVehicle`,
  {headers: new HttpHeaders({token : this.authService.jwt})
  }).pipe(
    tap(async (res: Vehicle) => {
      this.mat = res.mat;
      }, (err) => {
      if (err.status === 500) {this.alertServerError(); }
      if (err.status === 404) {
          this.mat = null;
           }
      })
  );
}

sendQR(qr: Qr): Observable<QrResponse> {

  return this.httpClient.post<Qr>(`${this.API_SERVER_ADDRESS}/sendQR`, qr,
  {headers: new HttpHeaders({token : this.authService.jwt})
  }).pipe(
    tap(async (res: QrResponse) => {
        this.alert('Codigo enviado!');
      }, (err) => {
      if (err.status === 500) {this.alertServerError(); }
      })
  );

}

sendReserve(reserve: Reserve): Observable<ReserveResponse> {
  console.log(reserve);
  return this.httpClient.post<Reserve>(`${this.API_SERVER_ADDRESS}/regReserve`, reserve,
  {headers: new HttpHeaders({token : this.authService.jwt})
  }).pipe(
    tap(async (res: ReserveResponse) => {
        this.alert('Reserva añadida!');
      }, (err) => {
      if (err.status === 500) {this.alertServerError(); }
      })
  );
}






  async alert(txt: string) {
    const alert = await this.alertControler.create({
      message: txt ,
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertServerError() {
    const alert = await this.alertControler.create({
      message: 'ERROR',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertMatriculaRegistrada(mat: string) {
    const alert = await this.alertControler.create({
      message: 'Matricula ' + mat + ' registrada' ,
      buttons: ['ok']
    });
    await alert.present();
  }
}
