import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/auth.service';
import { ApiService } from '../api.service';



@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.page.html',
  styleUrls: ['./add-vehicle.page.scss'],
})
export class AddVehiclePage implements OnInit {

  public mat = 'No registrado' ;

  constructor(private  router: Router, private  authService: AuthService, private apiService: ApiService) {
      this.getVehicle();
    }

    ionViewDidEnter() {
      console.log('chek log');
      this.authService.getToken().then(res => this.authService.checklog()) ;
    }
    back() {
      this.router.navigateByUrl('tabs');
    }
    vehicle(form) {
      this.apiService.regVehicle(form.value).subscribe((res) => {
       this.router.navigateByUrl('tabs');
            }
            );
    }

    getVehicle() {
      this.apiService.getVehicle().subscribe((res) => {
          this.mat = res.mat;
            },
            (err) => {

            });
    }

  ngOnInit() {
  }

}
