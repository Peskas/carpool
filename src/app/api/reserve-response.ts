

export interface ReserveResponse {
    mat: string;
    hours: number;
    minutes: number;
    state: boolean;
}
