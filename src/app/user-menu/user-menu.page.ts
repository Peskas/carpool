import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { ApiService } from '../api/api.service';
import { Reserve } from '../api/reserve';
import { ReserveResponse } from '../api/reserve-response';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.page.html',
  styleUrls: ['./user-menu.page.scss'],
})
export class UserMenuPage implements OnInit {

  reserva: ReserveResponse ;
  mat = null;
  createdReserve = null;

  constructor(private router: Router, private authService: AuthService, private apiService: ApiService) {
    this.getVehicle();
  }


  ngOnInit() {
    console.log('buscando vehiculo');
    this.getVehicle();
  }

  ionViewDidEnter() {
    console.log('chek log');
    this.authService.getToken().then(res => this.authService.checklog()) ;
    console.log('buscando vehiculo');
    this.getVehicle();
  }

   reserve(form) {
    if (this.createdReserve === true) {
      console.log('reserva ya creada');
    } else {
      console.log('creando reserva');
      if (this.mat === null) {
        console.log('No vehiculo registrado');
      } else {
        form.value.mat = this.mat;
        this.apiService.sendReserve(form.value).subscribe((res) => {
          console.log('reserva creada');
          console.log(res);

          this.reserva = res;
          console.log( this.reserva);
          this.createdReserve = true;
        },
          (err) => {
            console.log('reserva no creada');
          });
      }
    }
  }

  deleteReserve() {
    this.createdReserve = null;
  }

  getVehicle() {
    this.apiService.getVehicle().subscribe((res) => {
      console.log(res.mat);
      this.mat = res.mat;
    },
      (err) => {
        console.log('err');
      });
  }

}
