import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.page.html',
  styleUrls: ['./change-pass.page.scss'],
})
export class ChangePassPage implements OnInit {

  constructor(private  authService: AuthService, private  router: Router) { }

  ionViewDidEnter() {
    console.log('chek log');
    this.authService.getToken().then(res => this.authService.checklog()) ;
  }

  changePass(form) {
    this.authService.changePass(form.value).subscribe((res) => {
      console.log('loged in');
    });
  }

  back() {
    this.router.navigateByUrl('tabs');
  }

  ngOnInit() {
  }

}
