import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecoverPassCodePage } from './recover-pass-code.page';

const routes: Routes = [
  {
    path: '',
    component: RecoverPassCodePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecoverPassCodePage]
})
export class RecoverPassCodePageModule {}
