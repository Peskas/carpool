import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-recover-pass-code',
  templateUrl: './recover-pass-code.page.html',
  styleUrls: ['./recover-pass-code.page.scss'],
})
export class RecoverPassCodePage implements OnInit {

  constructor(private  authService: AuthService, private  router: Router) { }


  recoverPassCode(form) {
    this.authService.recoverPassCode(form.value).subscribe((res) => {
    });
  }

  back() {
    this.router.navigateByUrl('forget-pass');
  }

  ngOnInit() {
  }

}
