import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverPassCodePage } from './recover-pass-code.page';

describe('RecoverPassCodePage', () => {
  let component: RecoverPassCodePage;
  let fixture: ComponentFixture<RecoverPassCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoverPassCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverPassCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
