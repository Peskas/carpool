import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private  authService: AuthService, private  router: Router) { }

  login(form) {
    this.authService.login(form.value).subscribe((res) => {
      console.log('loged in');
      this.router.navigateByUrl('tabs');
    });
  }
  getToken() {
    this.authService.getToken();
  }

  devlogin() {
    this.authService.setToken();
  }

  ngOnInit() {
  }

}
