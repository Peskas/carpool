import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forget-pass',
  templateUrl: './forget-pass.page.html',
  styleUrls: ['./forget-pass.page.scss'],
})
export class ForgetPassPage implements OnInit {

  constructor(private  authService: AuthService, private  router: Router) { }


  recoverPass(form) {
    this.authService.recoverPass(form.value).subscribe((res) => {
      console.log(' recover recived');
      this.router.navigateByUrl('recover-pass-code');
    });
  }

  back() {
    this.router.navigateByUrl('login');
  }
  ngOnInit() {
  }

}
