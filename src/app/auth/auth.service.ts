import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

import { User } from './user';
import { AuthResponse } from './auth-response';
import { Code } from './code';
import { CodeResponse } from './code-response';
import { FormPass } from './form-pass';




@Injectable({
  providedIn: 'root'

})
export class AuthService {

public User: User = null;
public jwt: string;
public AUTH_SERVER_ADDRESS  =  'http://localhost:3000';
public authSubject  =  new  BehaviorSubject(false);

constructor( private  httpClient: HttpClient,
             private  storage: Storage,
             private  router: Router,
             private  alertControler: AlertController
             ) {}




  register(user: User): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${this.AUTH_SERVER_ADDRESS}/register`, user).pipe(
      tap(async (res: AuthResponse ) => {

        if (res.user) {
          await this.storage.set('ACCESS_TOKEN', res.access_token);
          await this.storage.set('EXPIRES_IN', res.expires_in);
          this.jwt = res.access_token;
          this.authSubject.next(true);
        }
      }, (err) => {
        if (err.status === 400) {this.alertPassDontMatch(); }
        if (err.status === 401) {this.alertEmailRegister(); }
        if (err.status === 402) {this.alertEmailNotValid(); }
        if (err.status === 500) {this.alertServerError(); }
      })
    );
  }


  registerCode(code: Code): Observable<CodeResponse> {

    // console.log("jwt var " + this.jwt);
    return this.httpClient.post<CodeResponse>(`${this.AUTH_SERVER_ADDRESS}/registerCode`, code,
     {headers: new HttpHeaders({token : this.jwt})
    }).pipe(

      tap(async (res: CodeResponse) => {
            console.log('valid');
            this.router.navigateByUrl('tabs');
      }, (err) => {
        console.log('not valid');
        if (err.status === 401) {this.alertCodeNotValid(); }
        if (err.status === 500) {this.alertServerError(); }
      })
    );
  }


  login(user: User): Observable<AuthResponse> {
    return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/login`, user).pipe(
      tap(async (res: AuthResponse) => {

        if (res.user) {
          console.log(res);
          console.log('token: ' + res.access_token);
          console.log('expire: ' + res.expires_in);
          await this.storage.set('ACCESS_TOKEN', res.access_token);
          await this.storage.set('EXPIRES_IN', res.expires_in);
          this.jwt = res.access_token;
          this.authSubject.next(true);
        }
      }, (err ) => {
        console.log(err.status);
        if (err.status === 401) {this.alertWrongPass(); }
        if (err.status === 404) {this.alertUserNotFound(); }
        if (err.status === 500) {this.alertServerError(); }
          }
      )
    );
  }


  async logout() {
    await this.storage.remove('ACCESS_TOKEN');
    await this.storage.remove('EXPIRES_IN');
    this.jwt = null;
    this.authSubject.next(false);
  }


  isLoggedIn() {
    return this.authSubject.asObservable();
  }


  recoverPass(user: User) {

    return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/recoverPass`, user).pipe(
      tap(async (res) => {
        this.alertSendCode(user.email);
      },
       (err) => {
        if (err.status === 404) {this.alertUserNotFound(); }
        if (err.status === 500) {this.alertServerError(); }
        console.log('error recover');
          })
      );
   }


   recoverPassCode(code: Code) {

    return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/recoverPassCode`, code).pipe(
      tap(async (res) => {
        this.alert('Contraseña restablecida');
        this.router.navigateByUrl('login');
      },
       (err) => {
        if (err.status === 404) {this.alertCodeNotValid(); }
        if (err.status === 500) {this.alertServerError(); }
        console.log('error recover code');
          })
      );
   }


  changePass(formPass: FormPass) {

    return this.httpClient.post(`${this.AUTH_SERVER_ADDRESS}/setPass`, formPass ,
    {headers: new HttpHeaders({token : this.jwt})}).pipe(
      tap(async (res) => {

       this.alertPassChange();
      },
       (err) => {
        if (err.status === 400) {this.alertPassDontMatch(); }
        if (err.status === 401) {this.alertWrongPass(); }
        if (err.status === 404) {this.alertUserNotFound(); }
        if (err.status === 500) {this.alertServerError(); }
        console.log('error recover');
          })
      );
  }


  async getToken() {
    await this.storage.get('ACCESS_TOKEN').then((jwt) => {
      if (jwt != null) {
        console.log('Token: ' + jwt);
        this.jwt = jwt;
        this.authSubject.next(true);
      } else { console.log('No Token (null)'); }
    },
    (err) => { console.log('error get token'); }
    );

    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  async checklog() {
    await this.isLoggedIn().subscribe((res) => {
      if (res) {
        console.log('access');
      }
      if (!res) {
        console.log('not access');
        this.router.navigateByUrl('login');
      }
     });
  }

  getUser(): Observable<User> {

    return this.httpClient.get<User>(`${this.AUTH_SERVER_ADDRESS}/getUser`,
    {headers: new HttpHeaders({token : this.jwt})
    }).pipe(
      tap(async (res: User) => {
        this.User = res;
        }, (err) => {
        if (err.status === 500) {this.alertServerError(); }
        })
    );
  }




 async alertServerError() {
    const alert = await this.alertControler.create({
      message: 'ERROR',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertWrongPass() {
    const alert = await this.alertControler.create({
      message: 'Contraseña Incorrecta',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertUserNotFound() {
    const alert = await this.alertControler.create({
      message: 'Email no Registrado!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertCodeNotValid() {
    const alert = await this.alertControler.create({
      message: 'Codigo Incorrecto!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertEmailRegister() {
    const alert = await this.alertControler.create({
      message: 'Email ya Registrado!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertPassDontMatch() {
    const alert = await this.alertControler.create({
      message: 'Contraseña no Coinicide!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertEmailNotValid() {
    const alert = await this.alertControler.create({
      message: 'Email incorrecto!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertPassChange() {
    const alert = await this.alertControler.create({
      message: 'Cotraseña Cambiada!',
      buttons: ['ok']
    });
    await alert.present();
  }
  async alertSendCode(txt: string) {
    const alert = await this.alertControler.create({
      message: 'Codigo enviado a: ' + txt ,
      buttons: ['ok']
    });
    await alert.present();
  }
  async alert(txt: string) {
    const alert = await this.alertControler.create({
      message: txt ,
      buttons: ['ok']
    });
    await alert.present();
  }




  // dev

  setToken() {
    // tslint:disable-next-line:max-line-length
    this.jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTQsImlhdCI6MTU3MTYyNDM5NywiZXhwIjoxNTcxNzEwNzk3fQ.MKZJkodX8LvoEiKrX6mQqBQzpgQR_oSFSrS3l8jP1x8';
    this.storage.set('ACCESS_TOKEN', this.jwt);
    this.authSubject.next(true);
  }

}
