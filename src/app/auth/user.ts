export interface User {
  user: string;
    id: number;
    name: string;
    email: string;
    password: string;
    confirm: string;
}
