import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCodePage } from './register-code.page';

describe('RegisterCodePage', () => {
  let component: RegisterCodePage;
  let fixture: ComponentFixture<RegisterCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
