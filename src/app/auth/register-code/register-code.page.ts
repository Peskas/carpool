import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register-code',
  templateUrl: './register-code.page.html',
  styleUrls: ['./register-code.page.scss'],
})
export class RegisterCodePage implements OnInit {

  constructor(private  authService: AuthService, private  router: Router) { }

  registerCode(form) {
    this.authService.registerCode(form.value).subscribe((res) => {});
  }

  back() {
    this.router.navigateByUrl('register');
  }

  ngOnInit() {
  }

}
