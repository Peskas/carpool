export interface FormPass {
    password: string;
    new_password: string;
    confirm: string;
}

