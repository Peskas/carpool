import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public userName = '' ;

  constructor(
    private  authService: AuthService,
    private  router: Router,
    ) {
    }


  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {

  }


  ionViewDidEnter() {
    console.log('chek log');
    this.authService.getToken().then(res => this.authService.checklog()) ;
    this.getUser();

  }



  logout() {
    console.log('loged out');
    this.authService.logout();
  }

  getUser() {
    console.log('get user');
    this.authService.getUser().subscribe((res) => {
        this.userName = res.user.split(' ', 1)[0];

          },
          (err) => {
          });
  }


  changePass() {
    this.router.navigateByUrl('change-pass');
  }
  addVehicle() {
    this.router.navigateByUrl('add-vehicle');
  }



}
